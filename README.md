# Firebase WebRTC Signaling

> **NOTE: This is a hobby project with no expectation of support or maintenance.**

## Description

A WebRTC signaler that uses firestore to negotiate the peer connection. Follows the WebRTC [perfect negotiation pattern](https://developer.mozilla.org/en-US/docs/Web/API/WebRTC_API/Perfect_negotiation).

## Installation

```
npm install --save-dev @nystik/firewire-rtc
```

## Usage
Remember to initialize your firebase app before using the signaler.

```typescript
import { initializeApp } from 'firebase/app'

const firebaseConfig = {
    apiKey: '<API KEY>',
    ...
}

initializeApp(firebaseConfig)
```

```typescript
import { getSignaler } from 'firewire-rtc'

// Create a peer connection
const peer = new RTCPeerConnection()

// Create signaler for the peer connection and set wether this peer is polite or not (see: https://developer.mozilla.org/en-US/docs/Web/API/WebRTC_API/Perfect_negotiation)
const polite = false
const signaler = getSignaler(peer, polite)

// Create a negotiation
const negotiationId = signaler()
...
// Or connect to an existing negotiation
signaler('<negotiation-id>')
```

At this point all offers, answers, and ice candidates are handles by the signaler and we can add streams and data channels to the peer connection as we wish.

```typescript
...
stream.getTracks().forEach((track) => peer.addTrack(track, stream))
...
```

## Known Issues

- if you get `Error: No Firebase App '[DEFAULT]' has been created - call Firebase App.initializeApp()` you need to tell your bundler to remove duplicate imports. For rollup add `dedupe: ['firebase']` to the node-resolve plugin options.

## License

This project is licensed under the MIT license, see [LICENSE](https://gitlab.com/Nystik/firewire-rtc/-/blob/main/LICENSE) for details.
