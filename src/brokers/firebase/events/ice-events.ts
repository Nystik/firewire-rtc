import { onSnapshot } from 'firebase/firestore'
import { getCandidatesRef } from '../utils/get-candidates-ref'
import { getDescriptionsRef } from '../utils/get-descriptions-ref'

type CandidateHandler = (candidate: RTCIceCandidate) => Promise<void> | void
type DescriptionHandler = (
    description: RTCSessionDescription
) => Promise<void> | void

const registerCandidateHandler =
    (currentBrokerId: string) =>
    (negotiationId: string) =>
    (handler: CandidateHandler) => {
        const ref = getCandidatesRef(negotiationId)

        onSnapshot(ref, (querySnapshot) => {
            querySnapshot.docChanges().forEach(async (change) => {
                if (change.type === 'added') {
                    const { brokerId, ...candidate } = change.doc.data()
                    if (brokerId != currentBrokerId) {
                        await handler(new RTCIceCandidate(candidate))
                    }
                }
            })
        })
    }

const registerDescriptionHandler =
    (currentBrokerId: string) =>
    (negotiationId: string) =>
    (handler: DescriptionHandler) => {
        const ref = getDescriptionsRef(negotiationId)

        onSnapshot(ref, (querySnapshot) => {
            querySnapshot.docChanges().forEach(async (change) => {
                if (change.type === 'added') {
                    const { brokerId, ...description } = change.doc.data()
                    if (brokerId != currentBrokerId) {
                        await handler(
                            new RTCSessionDescription(
                                description as RTCSessionDescriptionInit
                            )
                        )
                    }
                }
            })
        })
    }

export {
    registerCandidateHandler,
    registerDescriptionHandler,
    CandidateHandler,
    DescriptionHandler,
}
