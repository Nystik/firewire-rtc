import { addDoc } from 'firebase/firestore'
import { CandidateInfo, SimpleRTCSessionDescription } from './types'
import { v4 as uuid } from 'uuid'
import { createNegotiation } from './utils'
import { getCandidatesRef } from './utils'
import { registerCandidateHandler, registerDescriptionHandler } from './events'
import { getDescriptionsRef } from './utils/get-descriptions-ref'

interface FirebaseBroker {
    negotiationId: string
    send: (msg: BrokerMessage) => Promise<void> | void
}

interface BrokerMessage {
    description?: RTCSessionDescription | null
    candidate?: RTCIceCandidate | null
}

type MessageHandler = (msg: BrokerMessage) => Promise<void> | void

const sendIceCandidate =
    (brokerId: string) =>
    (negotiationId: string) =>
    async (candidate: RTCIceCandidate) => {
        const ref = getCandidatesRef(negotiationId)
        const candidateData = candidate.toJSON() as CandidateInfo
        await addDoc(ref, { ...candidateData, brokerId })
    }

const sendDescription =
    (brokerId: string) =>
    (negotiationId: string) =>
    async (description: RTCSessionDescription) => {
        const ref = getDescriptionsRef(negotiationId)
        const descriptionData =
            description.toJSON() as SimpleRTCSessionDescription
        await addDoc(ref, { ...descriptionData, brokerId })
    }

const getSend =
    (brokerId: string) =>
    (negotiationId: string) =>
    ({ description, candidate }: BrokerMessage) => {
        if (description) {
            sendDescription(brokerId)(negotiationId)(description)
        } else if (candidate) {
            sendIceCandidate(brokerId)(negotiationId)(candidate)
        }
    }

const getBroker =
    (onMessage: MessageHandler) =>
    async (negotiationId?: string): Promise<FirebaseBroker> => {
        const brokerId = uuid()

        const currentNegotiation = negotiationId || (await createNegotiation())

        registerDescriptionHandler(brokerId)(currentNegotiation)(
            (description) => onMessage({ description })
        )
        registerCandidateHandler(brokerId)(currentNegotiation)((candidate) =>
            onMessage({ candidate })
        )

        return {
            negotiationId: currentNegotiation,
            send: getSend(brokerId)(currentNegotiation),
        }
    }

export { getBroker, FirebaseBroker, MessageHandler }
