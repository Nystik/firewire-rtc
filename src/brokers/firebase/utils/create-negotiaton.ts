import { collection, addDoc, getFirestore } from 'firebase/firestore'

const createNegotiation = async (): Promise<string> => {
    const ref = collection(getFirestore(), `RTCNegotiations`)
    const doc = await addDoc(ref, {})

    return doc.id
}

export { createNegotiation }
