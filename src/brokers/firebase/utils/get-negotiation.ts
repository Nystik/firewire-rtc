import { doc, getDoc, getFirestore } from 'firebase/firestore'

const getNegotiationDoc = async (negotiationId: string): Promise<string> => {
    const db = getFirestore()

    const docRef = doc(db, 'RTCNegotiations', negotiationId)

    const docSnap = await getDoc(docRef)

    //const doc = await getDoc(docRef)

    return docSnap.id
}

export { getNegotiationDoc }
