import { FirebaseBroker, getBroker } from './brokers/firebase'

interface SignalMessage {
    description?: RTCSessionDescription | null
    candidate?: RTCIceCandidate | null
}

interface SignalerState {
    polite: boolean
    makingOffer: boolean
    broker: FirebaseBroker | null
}

const createState = (polite: boolean): SignalerState => ({
    polite,
    makingOffer: true,
    broker: null,
})

const isOfferCollison =
    (conn: RTCPeerConnection, makingOffer: boolean) =>
    (description: RTCSessionDescription) => {
        return (
            description.type == 'offer' &&
            (makingOffer || conn.signalingState != 'stable')
        )
    }

const getMessageHandler =
    (state: SignalerState) => (conn: RTCPeerConnection) => {
        let ignoreOffer = false
        return async ({ description, candidate }: SignalMessage) => {
            if (description) {
                ignoreOffer =
                    !state.polite &&
                    isOfferCollison(conn, state.makingOffer)(description)

                if (ignoreOffer) {
                    return
                }

                await conn.setRemoteDescription(description)

                if (description.type == 'offer' && state.broker) {
                    await conn.setLocalDescription()
                    state.broker.send({
                        description: conn.localDescription,
                    })
                }
            } else if (candidate) {
                try {
                    await conn.addIceCandidate(candidate)
                } catch (err) {
                    if (!ignoreOffer) {
                        throw err
                    }
                }
            }
        }
    }

const registerNegotiationNeededHandler =
    (state: SignalerState) => (conn: RTCPeerConnection) => {
        conn.onnegotiationneeded = async () => {
            try {
                state.makingOffer = true
                await conn.setLocalDescription()
                state.broker!.send({
                    description: conn.localDescription,
                })
            } catch (err) {
                console.error(err)
            } finally {
                state.makingOffer = false
            }
        }
    }

const registerIceCandidateHandler =
    (state: SignalerState) => (conn: RTCPeerConnection) => {
        conn.onicecandidate = ({ candidate }) =>
            state.broker!.send({ candidate })
    }

const getSignaler =
    (conn: RTCPeerConnection, polite: boolean) =>
    async (negotiationId?: string) => {
        const state = createState(polite)

        const onMessage = getMessageHandler(state)(conn)

        registerNegotiationNeededHandler(state)(conn)
        registerIceCandidateHandler(state)(conn)

        state.broker = await getBroker(onMessage)(negotiationId)

        return state.broker.negotiationId
    }

export { getSignaler }
